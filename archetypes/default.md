---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
lastmod:
categories: articles
draft: true
---

<div class="alert alert-info" role="alert">
  <strong>Work in progress!</strong> This blog post is still a draft and unfinished work. Content may be inaccurate or entirely wrong and subject to change.
</div>
