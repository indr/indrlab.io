# indr.gitlab.io

- `hugo` builds the site to public/.
- `hugo server -D` builds the site with drafts and serves it at 127.0.0.1:1313 in watch mode.

## Copyright

Copyright (c) 2018 Reto Inderbitzin
