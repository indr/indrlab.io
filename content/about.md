---
title: "Hi, I'm Reto!"
type: page
date: 2018-07-11T18:31:43+08:00
draft: true
---

I'm a full-stack software developer and traveler from Switzerland. I'm passionate about building web and mobile applications. Check out my current and recent projects. Sometimes I also post about software engineering or computing in general.

If you would like to get in touch, send me a message by email at mail@indr.ch which I promise to respond to.

## Projects

[335 Navigator App](/projects/335/) is a free Android app designed for blind and visually impaired. It provides an easy navigation using a simple content structure to listen to over 10,000 online radio stations as well as media on the device. It is ad free and supported by its users.

[phundus](/projects/phundus/) is a collaborative equipment rental and inventory management system that aims to connect honorary organisations and associations of public utility which provide reasonably priced equipment for leisure activities, culture and sports.
