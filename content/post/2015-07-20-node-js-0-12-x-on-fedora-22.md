---
title: "Node.js 0.12.x on Fedora 22"
date: 2015-07-20T14:45:00+00:00
categories: notes
tags: 
- fedora
- nodejs
---

No Node.js 0.12 build on fedora packages:  
[https://apps.fedoraproject.org/packages/nodejs/builds/](https://apps.fedoraproject.org/packages/nodejs/builds/).

Bug on Red Hat Bugzilla:  
[Bug 1192647 - 0.12 is released but fedora still using 0.10.xxx](https://bugzilla.redhat.com/show_bug.cgi?id=1192647)

How do I install Nodejs v.0.12.* on Fedora 21?  
[https://ask.fedoraproject.org/en/question/66233/how-do-i-install-nodejs-v012-on-fedora-21/](https://ask.fedoraproject.org/en/question/66233/how-do-i-install-nodejs-v012-on-fedora-21/)

Install npm packages globally without sudo on OS X and Linux:  
[https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md](https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md)
