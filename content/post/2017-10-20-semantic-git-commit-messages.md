---
title: "Semantic Git commit messages"
date: 2017-10-20T12:11:00+00:00
categories: notes
tags: 
- git
- vcs
---

[Karma - Git Commit Msg](http://karma-runner.github.io/0.10/dev/git-commit-msg.html)

[Semantic Commit Messages](https://seesparkbox.com/foundry/semantic_commit_messages)

[Git semantic commits](https://github.com/fteem/git-semantic-commits)
