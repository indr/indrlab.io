---
title: "Error: [vuex] vuex requires a Promise polyfill in this browser"
date: 2017-09-13T12:59:00+00:00
categories: notes
tags:
- vuejs
- vuex
---
An error message everyone finds himself surprised to see once you start unit testing Vue components which have a dependency to vuex:

```
PhantomJS 2.1.1 (Linux 0.0.0) ERROR
  Error: [vuex] vuex requires a Promise polyfill in this browser.
  at webpack:///~/vuex/dist/vuex.esm.js:96:0 <- index.js:54757
```

Searching the web for the error message quickly unveils a [solution on Stack Overflow](https://stackoverflow.com/questions/40784305/vue-unit-test-error-vuex-requires-a-promise-polyfill-in-this-browser):

Install Babel Polyfill:

```bash
npm install --save-dev babel-polyfill
```

then include the polyfill file before your source and test files within the files section of your karma.conf.js:

```js
files: [
  '../node_modules/babel-polyfill/dist/polyfill.js',
  'index.js'
],
```

More information and discussions about different solutions can be found in the GitHub repository of the webpack vuejs-template in issue [#260](https://github.com/vuejs-templates/webpack/issues/260) and [#474](https://github.com/vuejs-templates/webpack/issues/474).

