---
title: "How to run Vue.js unit tests in watch mode"
date: 2017-09-13T14:29:00+00:00
categories: articles
tags:
- vuejs
- karma
---

Running the whole set of unit tests with `npm run unit` everytime you make a change to a component takes several seconds. This definitely doesn't fit my "code a little, test a little" workflow. Before I wanted to dive into webpack and the karma setup to employ something like a watch mode, I searched the Internet and found [a neat solution](https://github.com/vuejs-templates/webpack/issues/875#issuecomment-326797505) to achieve the goal:

Karma, the spectacular test runner for JavaScript, has an `autoWatch` option, which I added to my `package.json` file like this:

```
"unit:watch": "cross-env BABEL_ENV=test karma start test/unit/karma.conf.js --auto-watch",
```

