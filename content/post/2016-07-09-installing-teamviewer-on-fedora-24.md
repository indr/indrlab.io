---
title: "Installing TeamViewer on Fedora 24 with SELinux"
date: 2016-07-09T15:55:00+00:00
categories: notes
tags:
- fedora
- selinux
- teamviewer
---

```
dnf install teamviewer_11.0.57095.i686.rpm
ausearch -c 'teamviewerd' --raw | audit2allow -M my-teamviewerd
semodule -X 300 -i my-teamviewerd.pp
systemctl start teamviewerd.service
```

```
systemctl stop teamviewerd.service
semodule -l | grep team
semodule -X 300 -r my-teamviewerd
dnf remove teamviewer
```

[I can not start TeamViewer after installation. (F24 alpha)](https://ask.fedoraproject.org/en/question/87123/i-can-not-start-teamviewer-after-installation-f24-alpha/?answer=89290#post-id-89290)
