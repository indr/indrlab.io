---
title: "Scripting production-ready HTML templates for CasparCG"
date: 2019-02-03T13:46:00+00:00
categories: articles
tags:
- casparcg
- html producer
- webcg
draft: true
---

<div class="alert alert-info" role="alert">
  <strong>Work in progress!</strong> This blog post is still a draft and unfinished work. Content may be inaccurate or entirely wrong and subject to change.
</div>

## Handling, testing and debuggin AMCP commands

- Handling commands play(), stop(), next()
- Invoking commands in your browsers console
- Mention JavaScript debugger in browser.

## Parsing incoming data

- How to parse incoming data
- Don't reinvent the wheel: Easy peacy with webcg-framework

## Introducing webcg-framework

- Introduction of webcg-framework
- Why you should use it:
  - Multiple subscriptions
  - Error handling
  - Automatic parsing
  - State machine, idempotency
- Last but not least: webcg-devtools

## Introducing webcg-devtools

- Using lazy-loaded webcg-devtools

## Wrapping up


[Join the discussion in the official CasparCG forum](https://casparcgforum.org/t/introduction-to-casparcgs-html-producer/1386).
