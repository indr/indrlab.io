---
title: "Loading and playing websites from arbitrary URLs with CasparCG"
date: 2019-02-18T08:43:40+08:00
lastmod:
categories: notes
tags:
- casparcg
- html producer
---

Pass an URL while initializing a new HTML producer on a video layer:

```
PLAY 1-10 [HTML] "https://www.indr.ch/"
```

Or load the website as a template starting from server version 2.2.0 Beta 7:

```
CG 1 ADD 0 "https://www.indr.ch/" 1
```

Keep in mind that this will trigger the `update()` and `play()` functions.


If you use an older server version and you can only issue CG commands (such as from a client software), you have to load a template that does a redirect. Use a [Meta refresh](https://en.wikipedia.org/wiki/Meta_refresh) if you always want to load the same website:

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>load-url.html</title>
    <meta http-equiv="refresh" content="0; url=https://www.indr.ch/">
</head>
</html>

```

If you dynamically want to play different URLs, you need to navigate with JavaScript to the URL that is passed either in the component XML data or JSON object format to the template. Use the [webcg-framework](https://github.com/indr/webcg-framework) for easily parsing the raw string and then navigate like this:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>load-url.html</title>
    <script src="webcg-framework.umd.js"></script>
    <script type="text/javascript">
      webcg.on('data', function (data) {
        // Get the URL either from component XML data or JSON object format
        const url = data.f0.text || data.f0
        // Navigate to the new page
        window.location = url
      })
    </script>
</head>
</html>
```

Load and play the template with component XML data:
```
CG 1 ADD 0 "load-url" 1 "<templateData><componentData id=\"f0\"><data id=\"text\" value=\"https://www.indr.ch/\"/></componentData></templateData>"
```

Or, starting from server version 2.1.0 Beta 1, with the JSON object format:
```
CG 1 ADD 0 "load-url" 1 "{\"f0\":\"https://www.indr.ch/\"}"
```
