---
title: "Sign a specific APK with Bitrise's sign-apk Step"
date: 2017-03-28T23:56:00+00:00
categories: articles
tags:
- android
- bitrise
---

[Bitrise](https://www.bitrise.io/) (iOS & Android Build Automation), lets you automatically sign your apk files and offers you [two options](http://devcenter.bitrise.io/android/code-signing/) to achieve this: You either specify code sign configuration in your Gradle configuration, or you let [sign-apk step](https://github.com/bitrise-steplib/steps-sign-apk) sign your unsigned apk. Unfortunately, if you want to build more than one build variant or you have different platform variants or flavors, you can't specify which apk the sign-apk step should pick up and sign. There is already an issue reported for [support of multiple apk in android workflows](https://discuss.bitrise.io/t/support-for-multiple-apk-in-android-workflows/155).

For [3 3 5](/3-3-5), I didn't want to wait until Bitrise provides an out-of-box solution, but at the same time I wanted to:

 * Build a debug and a release apk,
 * Automatically sign the release apk during the build process,
 * Not specify sign configuration in Gradle configuration, and
 * Rename `release-unsigned-bitrise-signed` to a nicer name.
  
I came up with two little shell scripts I placed before and after the sign-apk step in the build workflow. The before script "Set APK path for signing" looks for the unsigned apk file and sets an environment variable with its file path.

```bash
#!/bin/bash
# fail if any commands fails
set -e
# debug log
set -x

# find *unsigned*.apk in directory of BITRISE_APK_PATH
MY_APK_PATH=`find "\`dirname $BITRISE_APK_PATH\`" -type f -path "*unsigned*.apk" | head -n 1`

# add env var MY_APK_PATH
envman add --key MY_APK_PATH --value "$MY_APK_PATH"
echo "The unsigned apk path is now available in the environment variable: \$MY_APK_PATH (value: $MY_APK_PATH)"

# optional: print file info
file $BITRISE_APK_PATH
file $MY_APK_PATH

# optional: replace BITRISE_APK_PATH
#envman add --key BITRISE_APK_PATH --value "$MY_APK_PATH"
```

In order to not surprise myself in the future, I set the environment variable `$MY_APK_PATH` which is then used in the sign-apk step as an input value:

![sign-apk step apk path](/img/bitrise-sign-apk-step-apk-path.png)

The after script "Rename, remove APKs" cleans up the deploy directory after the sign-apk step.

```bash
#!/bin/bash
# fail if any commands fails
set -e
# debug log
set -x

# rename apk using prename
#rename -V
# Error: Bareword "bitrise" not allowed while "strict subs" in use at (user-supplied code).
# Error: Bareword "signed" not allowed while "strict subs" in use at (user-supplied code).
#rename -v -- "-unsigned-bitrise-signed" "-signed" $BITRISE_DEPLOY_DIR/*.apk

# rename apk using mv
for i in $BITRISE_DEPLOY_DIR/*-unsigned-bitrise-signed.apk; do mv $i ${i/-unsigned-bitrise-signed/-bitrise-signed}; done

# remove unsigned apks
rm $BITRISE_DEPLOY_DIR/*unsigned*.apk
```

There was a little surprise when `rename` threw errors about not allowed words in a regular expression. Apperantly, `rename` on Debian is `prename`. The supplied expression is being executed as a perl script and there must be some conflict with imported libraries and symbols. Didn't investigate that any further and instead used `mv` in a for loop.

![Bitrise Apps and Artifacts](/img/bitrise-apps-and-artifacts-release-bitrise-signed.png)
