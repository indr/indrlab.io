---
title: "Migrating from ember-cli-qunit to ember-cli-mocha"
date: 2016-08-08T10:54:00+00:00
categories: notes
tags:
- ember
- mocha
- qunit
---

Sloppy bash script to migrate tests written with `qunit` to `mocha`.

```bash
# Status
node --version && npm --version && bower --version && phantomjs --version
# v6.3.1
# 3.10.3
# 1.7.9
# 2.1.1
ember --version
# ember-cli: 2.6.3
# node: 6.3.1
# os: linux x64
ember test
# tests 112, pass 112, skip 0, fail 0

# Install ember-cli-mocha
ember install ember-cli-mocha
# ? Overwrite tests/test-helper.js? (Yndh) Y
rm -rf dist bower_components node_modules tmp
npm install && bower install
ember test
# tests 108, pass 77, skip 0, fail 31
cd tests

# Fix imports
grep -rl "(assert)" | xargs sed -i "1s/^/import { assert } from 'chai';\n/";
grep -rl "'ember-qunit';" | xargs sed -i "s/'ember-qunit';/'ember-mocha';/g"
sed -i "s/import { assert } from 'chai';//" test-helper.js
grep -rl "moduleForComponent" | xargs sed -i "s/moduleForComponent/describeComponent/g"
grep -rl "moduleForModel" | xargs sed -i "s/moduleForModel/describeModel/g"
grep -rl "moduleFor" | xargs sed -i "s/moduleFor/describeModule/g"
grep -rl " test }" | xargs sed -i "s/ test }/ it }/g"
grep -rl " test}" | xargs sed -i "s/ test}/ it }/g"
grep -rl ", test," | xargs sed -i "s/, test,/, it,/g"
grep -rlE "'qunit'" | xargs sed -i "s/module/describe/g"
grep -rlE "'qunit'" | xargs sed -i "s/'qunit'/'mocha'/g"

# Fix test(... => it(
grep -rl "test(" | xargs sed -i "s/test(/it(/g"

# Fix `assert`
grep -rl "function(assert)" | xargs sed -i "s/function(assert)/function (assert)/g"
grep -rl "function (assert)" | xargs sed -i "s/function (assert)/function ()/g"
grep -rl "assert.expect(" | xargs sed -i '/assert.expect(/d'
grep -rl "assert.async()" | xargs sed -i '/assert.async()/d'

# Fix describe();
grep -rlE "describe\('[^']+'\);" | xargs sed -i "0,/');/{s/);/, function () {/}"

# Encapsule all it()'s in function () { ... } for module()
grep -rl "it(" | xargs sed -i "0,/^});/{s/});/  },\n  function () {/}"
grep -rl "it(" | xargs sed -i "\$a});"
grep -rlE "describe\('" | xargs sed -i "\$a});"

# Fix "'done' is not defined" in these files manually by adding `done` as the tests first argument
grep -r "done()"

# Fix these references to `qunit` manually. They my contain things like changing test timeout etc.
grep -ri "qunit"

cd ..
ember test
# tests 112, pass 112, skip 0, fail 0 
```
:+1:

## Error: Can't find variable: QUnit

Took me hours to figure out I should completely reinstall the node and bower packages:

```bash
rm -rf dist bower_components node_modules tmp
npm install && bower install
```

Interesting that things like these made a difference to the generated /dist/assets/tests.js

```js
// Didn't work:
import Ember from 'ember';

export default Ember.Service.extend(Ember.Evented, {
  // ...
});

// Worked:
import Ember from 'ember';

const Evented = Ember.Evented;

export default Ember.Service.extend(Evented, {
  // ...
});
```

```js
// Didn't work:
import Ember from 'ember';

const {
  RSVP
} = Ember;

// Worked:
import Ember from 'ember';

const RSVP = Ember.RSVP;
```
