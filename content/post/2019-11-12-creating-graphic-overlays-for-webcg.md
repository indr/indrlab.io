---
title: "Creating graphic overlays for WebCG"
date: 2019-11-12T12:44:48+08:00
lastmod:
categories: articles
tags:
 - webcg
---

This article provides a brief introduction into creating graphic overlays for WebCG. In-depht information, explanations and best practices for creating production-ready graphic overlays for both WebCG and CasparCG can be found in the series [Introduction to CasparCG’s HTML producer](/2018/09/introduction-to-casparcgs-html-producer/).

Graphic overlays for WebCG are plain HTML documents that conform to a JavaScript interface. This interface consists of four functions that are defined in the global namespace (the window object). The WebCG host calls these functions to communicate and instruct the graphic overlays. These functions are defined as follows:

| Name | Description |
| ---- | ----------- |
| play() | Called when the graphic overlay should start playing. This normally triggers an in animation. |
| stop() | Called when the graphic overlay should stop playing. This normally triggers an out animation. |
| next() | Called when the graphic overlay should transition to the next logical part of the graphic overlay. This may be used to switch between two different displays of the same data set. |
| update(data: string) | Called to transfer data to the graphic overlay. This may be called before, during and after the overlay is playing. The argument `data` is a JSON string that has to be parsed by the graphic overlay. |

## Basic example

The follow HTML document is a basic example to demonstrate the interaction between the WebCG host and the graphic overlay. Read on to learn about the recommended way to create graphic overlays using the webcg-framework.

```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Basic example</title>
</head>
<body>  
  <div id="overlay" style="background-color:white;opacity:0;transition:opacity 1s">
    <h1>Basic example</h1>
  </div>
  <p>Open your browser's developer tools and call 'play()' and 'stop()' in the JavaScript console.</p>
  <script>  
  function play() {
    document.getElementById('overlay').style.opacity = 1;
  }
    
  function stop() {
    document.getElementById('overlay').style.opacity = 0;
  }  
  </script>
</body>
</html>
```

As you can see, a graphic overlay is a web page. The HTML document above defines a simple element with an id “overlay” that contains a title, and two JavaScript functions to show and hide that element when the `play` command is executed, or the `stop` command respectively.

## webcg-framework

For the developer’s and the designer’s convenience, it is highly recommended to use the [webcg-framework](https://github.com/indr/webcg-framework). This framework provides a simple API to listen to the above mentioned functions, respectively commands, it ensures that the commands are only executed when the graphic overlay is in the correct state (e.g. it prevents the play command being executed multiple times consecutively), it takes care of the parsing of the data that is passed via the `update` command and it provides tools with a user interface to debug and test your graphic overlay.

Have a look at [the running examples online](http://indr.github.io/webcg-framework/) or read [the installation instructions](https://github.com/indr/webcg-framework#installation). This framework is also covered in the aforementioned guide. If you have any questions, don’t hesitate to contact me directly, I’m always happy to help.
