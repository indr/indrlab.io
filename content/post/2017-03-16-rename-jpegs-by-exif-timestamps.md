---
title: "Rename JPEGs by Exif Timestamps"
date: 2017-03-16T10:03:00+00:00
categories: notes
tags:
- exif
- exiv2
---

[exiv2(1)](https://linux.die.net/man/1/exiv2) is a tool for image metadata manipulation that also lets you rename files based on its metadata. From the manpage:

> mv \| rename  
>  
> Rename files and/or set file timestamps according to the Exif create timestamp. Uses the value of tag Exif.Photo.DateTimeOriginal or, if not present, Exif.Image.DateTime to determine the timestamp. The filename format can be set with -r fmt, timestamp options are -t and -T. 

To rename all JPEG files in a folder, use

```
exiv2 mv *.JPG
```

## Custom Filename Format

> -r fmt  
>  
> Filename format for the 'rename' action. The format string follows [strftime(3)](https://linux.die.net/man/3/strftime) and supports the following keywords:  
\:basename\: - original filename without extension  
\:dirname\: - name of the directory holding the original file  
\:parentname\: - name of parent directory  
Default filename format is %Y%m%d_%H%M%S. 

To only prefix the filenames with the timestamp:

```bash
exiv2 -r %Y%m%d_%H%M%S_:basename: mv *.JPG 
```

## Create an Alias in .bashrc

Too lazy to remember and type that fileformat, I try to remember `exiv2_mv *.JPG`:

```bash
# 16.03.17: added by me: rename filename based on exif metadata
exiv2_mv () {
  exiv2 -r %Y%m%d_%H%M%S_:basename: mv "$@"
}
```
