---
title: phundus
type: page
date: 2017-10-21T17:48:13+00:00
url: /phundus/
---

<img class="float-left" src="/img/phundus_web_hi_res_150.png" alt="phundus logo"/>

phundus is a collaborative equipment rental and inventory management system.

It aims to connect honorary organisations and associations of public utility which provide reasonably priced equipment for leisure activities, culture and sports. phundus is the plattform to present their equipment and make it available for rental to their members and optionally to the public.

The production environment hosted at [www.phundus.ch](https://www.phundus.ch/) serves as the plattform for institutions mentioned above and especially children and youth organisations such as [Jubla](https://en.wikipedia.org/wiki/Jungwacht_Blauring), [Scouts](https://en.wikipedia.org/wiki/Scouting), [YMCA](https://en.wikipedia.org/wiki/YMCA) and any other institution in the field of open youth work in Switzerland.

### Links

- Visit [www.phundus.ch](https://www.phundus.ch/)
- [phundus](https://github.com/indr/phundus) on GitHub
