
function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

function fetch(url, success, error) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.setRequestHeader('Cache-Control', 'no-cache');

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            if (typeof success === 'function') {
                success(this.response);
            }
        } else {
            // We reached our target server, but it returned an error
            if (typeof error === 'function') {
                error();
            }
        }
    };

    request.onerror = function () {
        // There was a connection error of some sort
        if (typeof error === 'function') {
            error();
        }
    };

    request.send()
}
